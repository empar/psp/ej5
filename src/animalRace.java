import java.util.ArrayList;

public class animalRace extends Thread{

    public static void main(String[] args) {
        tortoiseRace(4, 3000, 100, 20);
        rabbitRace(3, 3000, 100, 100);
        cheetahRace(2, 3000, 100, 300);
    }
public static void rabbitRace(int rabbits, int distance, int sleep,int step){

    ArrayList<rabbit> thread = new ArrayList<rabbit>();

    for (int i = 0; i<rabbits;i++){
        thread.add(new rabbit("rabbit"+(i+1),distance,sleep,step));
    }

    for(int i=0; i<rabbits;i++){
        thread.get(i).start();
    }
}


    public static void tortoiseRace(int tortoises, int distance, int sleep,int step){

        ArrayList<tortoise> thread = new ArrayList<tortoise>();

        for (int i = 0; i<tortoises;i++){
            thread.add(new tortoise("tortoise"+(i+1),distance,sleep,step));
        }

        for(int i=0; i<tortoises;i++){
            thread.get(i).start();
        }
    }


    public static void cheetahRace(int cheetahs, int distance, int sleep,int step){

        ArrayList<cheetah> thread = new ArrayList<cheetah>();

        for (int i = 0; i<cheetahs;i++){
            thread.add(new cheetah("cheetah"+(i+1),distance,sleep,step));
        }

        for(int i=0; i<cheetahs;i++){
            thread.get(i).start();
        }
    }
}
