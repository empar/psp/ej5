public class cheetah extends Thread{
    private  String name;
    private  int distance;
    private int sleep;
    private int step;

    public cheetah(String name ,int distance, int sleep, int step) {
        this.name= name;
        this.distance = distance;
        this.sleep = sleep;
        this.step= step;
    }
    @Override
    public void run() {
        for (int i=0; i<this.distance; i+=this.step){
            System.out.println(this.name+": "+(this.distance-i) +"m");
        }
        try{
            sleep(this.distance);
        }catch (InterruptedException e){
            interrupt();
        }
        System.out.println(this.name+": "+" Finished");
    }
}
